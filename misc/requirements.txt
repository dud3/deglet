flask
flask-restful
flask-cors
flask-httpauth
tornado
requests[security]
redis
rethinkdb

supervisor

git+https://github.com/petertodd/python-bitcoinlib/

pytest
