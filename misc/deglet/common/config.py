from cfg_deglet import SQLDB_PATH, RDB, RDB_TABLE
from cfg_deglet import BITCOIN_URL, BITCOIN_TESTNET
from cfg_deglet import COSIGNER_SERVER
from cfg_deglet import TRANSACTION
from cfg_deglet import COINAPULT_API

for key in ('duration', 'validfor', 'limit'):
    assert key in TRANSACTION, '%s not in %s' % (key, TRANSACTION)
