import rethinkdb as r
from rethinkdb.errors import RqlDriverError
from flask import Blueprint, g
from flask_restful import Api, Resource, reqparse
from flask.ext.httpauth import HTTPBasicAuth

from .auth import get_password
from ..common.config import RDB, RDB_TABLE
from ..common.generate_id import valid_id


RESULTS_PER_PAGE = 10
MAX_RESULTS = RESULTS_PER_PAGE * 10

limit_parser = reqparse.RequestParser()
# Argument "limit": maximum number of results to return.
limit_parser.add_argument('limit', type=int, trim=True, required=False,
                          location='args')
# Argument "from": include transactions that were created from this
# date and on. Format: YYYY-MM-DD
limit_parser.add_argument('from', type=str, trim=True, required=False,
                          location='args')
# Argument "to": include transactions that were created no later
# than this date. Format: YYYY-MM-DD
limit_parser.add_argument('to', type=str, trim=True, required=False,
                          location='args')
# Argument "id": filter transaction by a partial id
limit_parser.add_argument('id', type=str, trim=True, required=False,
                          location='args')
# Argument "payment_address": find transaction by exact address
limit_parser.add_argument('payment_address', type=str, trim=True, required=False,
                          location='args')

auth = HTTPBasicAuth()
auth.get_password(get_password)


def before_request():
    try:
        g.rdb_conn = r.connect(**RDB)
    except RqlDriverError:
        pass


def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


class History(Resource):

    decorators = [auth.login_required]

    def _validate_limit(self, args):
        limit = args['limit']
        if limit is None or limit <= 0:
            limit = RESULTS_PER_PAGE
        elif limit > MAX_RESULTS:
            limit = MAX_RESULTS
        return limit

    def _validate_from(self, args):
        from_date = args['from']
        if from_date:
            return self._format_date(from_date), True
        else:
            return r.time(2000, 1, 1, "Z"), False

    def _validate_to(self, args):
        to_date = args['to']
        if to_date:
            return self._format_date(to_date), True
        else:
            return r.now(), False

    def _format_date(self, date):
        if '-' in date:
            year, month, day = map(int, date.split('-'))
            return r.time(year, month, day, "Z")

        return r.epoch_time(float(date))

    def get(self, page):
        """
        Get last n transactions for a given uid starting on page i.
        """
        args = limit_parser.parse_args()
        limit = self._validate_limit(args)
        from_date, from_date_avail = self._validate_from(args)
        to_date, to_date_avail = self._validate_to(args)
        address = args['payment_address']
        partial_id = args['id']

        filtered = False

        uid = auth.username()
        if page <= 0:
            page = 1

        page = page - 1
        to_skip = page * RESULTS_PER_PAGE

        query = r.table(RDB_TABLE)
        if address:
            # When an address is specified, restrict transactions by that
            # and then filter by the user.
            filtered = True
            query = query.get_all(address, index='payment_address')
            query = query.filter(r.row['owner_id'].match(uid))
        else:
            # Get all transactions that belong to this user.
            query = query.get_all(uid, index='owner_id')

        # Filter based on a partial transaction id, if present.
        if partial_id and valid_id(partial_id, check_base_only=True):
            filtered = True
            query = query.filter(r.row['id'].match(partial_id))
        # Filter to those transactions between the date range specified.
        if from_date_avail or to_date_avail:
            filtered = True
            query = query.filter(
                r.row['status_info'].nth(-1)['date'].during(from_date, to_date))
        # Find what would be the total number of results if limit(..)
        # was not applied
        total = query.count().run(g.rdb_conn)

        # Order by creation date.
        query = query.order_by(r.desc(
            lambda trans: trans['status_info'].nth(-1)['date']))
        # Restrict columns returned.
        query = query.pluck('payment_address', 'input', 'output',
                            'status_info', 'conversion', 'id')
        # Skip & limit results.
        query = query.skip(to_skip).limit(limit)
        data = query.run(g.rdb_conn, time_format='raw')

        result = {
            'total': total,
            'page': page + 1,
            'filtered': filtered,
            'data': list(data)
        }

        return result


api_blueprint = Blueprint('history', __name__)
api_blueprint.before_request(before_request)
api_blueprint.teardown_request(teardown_request)

api = Api(api_blueprint)
api.add_resource(History, '/test/history/<int:page>')
