import rethinkdb as r

from ..common.config import RDB, RDB_TABLE


def setup():
    db = RDB.pop('db')
    conn = r.connect(**RDB)

    if db not in r.db_list().run(conn):
        r.db_create(db).run(conn)

    tables = r.db(db).table_list().run(conn)
    to_create = (RDB_TABLE, 'balance')
    for name in to_create:
        if name in tables:
            continue
        r.db(db).table_create(name).run(conn)

    indexes = r.db(db).table(RDB_TABLE).index_list().run(conn)
    to_create = ('owner_id', 'payment_address')
    for name in to_create:
        if name in indexes:
            continue
        r.db(db).table(RDB_TABLE).index_create(name).run(conn)
    r.db(db).table(RDB_TABLE).index_wait().run(conn)


if __name__ == "__main__":
    setup()
