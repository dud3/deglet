import sqlite3

from ..common.config import SQLDB_PATH


def setup():
    conn = sqlite3.connect(SQLDB_PATH)
    cursor = conn.cursor()

    cursor.execute("""
        CREATE TABLE IF NOT EXISTS walletblob (
            uid TEXT PRIMARY KEY,
            username TEXT NOT NULL UNIQUE,
            usdbalance INTEGER NOT NULL,
            blob TEXT UNIQUE,
            cowallet TEXT UNIQUE
        )""")

    cursor.execute("""
        CREATE TABLE IF NOT EXISTS blobupdate (
            timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            uid TEXT NOT NULL,
            blob TEXT,
            FOREIGN KEY(uid) REFERENCES walletblob(uid)
        )""")

    cursor.execute("""
        CREATE TABLE IF NOT EXISTS usdhistory (
            uid TEXT NOT NULL,
            amount INTEGER NOT NULL,
            tid TEXT,
            PRIMARY KEY (uid, tid),
            FOREIGN KEY(uid) REFERENCES walletblob(uid)
        )""")

    cursor.execute("""
        CREATE TRIGGER IF NOT EXISTS update_usdbalance
            AFTER INSERT ON usdhistory
            BEGIN
                UPDATE walletblob SET usdbalance = usdbalance + new.amount
                WHERE uid = new.uid;
            END
        """)

    cursor.execute("""
        CREATE TRIGGER IF NOT EXISTS update_blob
            BEFORE UPDATE OF blob ON walletblob
            BEGIN
                SELECT RAISE(ABORT, "Too many updates")
                WHERE (SELECT COUNT(*) FROM blobupdate WHERE uid = OLD.uid) > 2;

                INSERT INTO blobupdate (uid, blob) VALUES (OLD.uid, OLD.blob);
            END
        """)

    conn.commit()


if __name__ == "__main__":
    setup()
