#
# NOTE
# redis needs to be configured with:
#
# notify-keyspace-events Ex
#
# In order to capture the events used here.
# Edit redis.conf and make sure that setting is specified
# then restart redis-server.
#
# Expired keys are captured and then used to update the respective
# transaction status.
#

import redis
import rethinkdb as r

from ..common.transaction import update_status
from ..common.config import RDB, RDB_TABLE


def watch():
    rdb = r.connect(**RDB)

    red = redis.StrictRedis()
    pubsub = red.pubsub()
    pubsub.psubscribe('__keyevent@*__:expired')

    for item in pubsub.listen():
        print item
        if item['type'] == 'pmessage' and item['channel'].endswith('expired'):
            key = item['data']
            print 'key %s expired' % key,
            trans = r.table(RDB_TABLE).get(key).run(rdb, time_format='raw')
            if trans:
                print "updating transaction"
                update_status(trans, conn=rdb)
            else:
                print "no transaction"


if __name__ == "__main__":
    watch()
