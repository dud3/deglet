from deglet.api import convert_handler


def test_known_BTC_x_USD():
    # known BTC -> x USD: user will send BTC.
    params = {
        'side': 'from',
        'currency': 'BTC',  # currency relates to the side specified.
        'other_currency': 'USD'
    }
    assert convert_handler._user_sends(**params)


def test_x_BTC_known_USD():
    # x BTC -> known USD: user will send calculatd BTC.
    params = {
        'side': 'to',
        'currency': 'USD',
        'other_currency': 'BTC'
    }
    assert convert_handler._user_sends(**params)


def test_x_USD_known_BTC():
    # x USD -> known BTC: user will receive specified BTC.
    params = {
        'side': 'to',
        'currency': 'BTC',
        'other_currency': 'USD'
    }
    assert not convert_handler._user_sends(**params)


def test_known_USD_x_BTC():
    # known USD -> x BTC: will receive calculated BTC.
    params = {
        'side': 'from',
        'currency': 'USD',
        'other_currency': 'BTC'
    }
    assert not convert_handler._user_sends(**params)
