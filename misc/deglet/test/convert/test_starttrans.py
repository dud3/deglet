from decimal import Decimal

from deglet.common.transaction import TransactionStatus
from deglet.api import convert_handler


def check_status(trans):
    # Conversions automatically move from waiting_user to pending.
    assert len(trans['status_info']) == 2, trans
    assert trans['status_info'][0]['status'] == TransactionStatus.PENDING, trans
    assert trans['status_info'][1]['status'] == TransactionStatus.WAITING_USER, trans


def check_sent(trans):
    assert 'sent' in trans['input'], trans
    assert trans['input']['sent']['total'] == 0, trans
    assert trans['input']['sent']['tx'] == [], trans


def check_received(trans):
    assert 'received' in trans['output'], trans
    assert trans['output']['received']['total'] == 0, trans
    assert trans['output']['received']['tx'] == [], trans


# Convert 0.5 BTC to x USD.
def test_convert_known_BTC_USD():
    params = {
        'uid': '123', 'address': 'abc',  # Unused, not testing validation.
        'side': 'from',
        'amount': int(Decimal('0.5') * Decimal('1e8')),
        'currency': 'BTC',
        'other_currency': 'USD'
    }

    trans = convert_handler._start_trans(**params)
    check_sent(trans)
    check_status(trans)
    assert trans['input']['amount'] == params['amount']
    assert trans['input']['currency'] == params['currency']
    assert trans['output']['currency'] == params['other_currency']
    btcusd_rate = Decimal(trans['conversion']['rate'])
    output_amount = int(params['amount'] / Decimal('1e8') * btcusd_rate * Decimal('1e2'))
    assert trans['output']['amount'] == output_amount
    assert trans['output']['amount'] / 1e2 > 10  # Sanity check.


def test_convert_BTC_known_USD():
    # Convert x BTC to 10 USD.
    params = {
        'uid': '123', 'address': 'abc',
        'side': 'to',
        'amount': int(Decimal('10') * Decimal('1e2')),
        'currency': 'USD',
        'other_currency': 'BTC'
    }

    trans = convert_handler._start_trans(**params)
    check_sent(trans)
    check_status(trans)
    assert trans['output']['amount'] == params['amount']
    assert trans['output']['currency'] == params['currency']
    assert trans['input']['currency'] == params['other_currency']
    btcusd_rate = Decimal(trans['conversion']['rate'])
    input_amount = int(params['amount'] / Decimal('1e2') / btcusd_rate * Decimal('1e8'))
    assert trans['input']['amount'] == input_amount
    assert trans['input']['amount'] / 1e8 < 0.5


def test_convert_known_USD_BTC():
    # Convert 10 USD to x BTC.
    params = {
        'uid': '123', 'address': 'abc',
        'side': 'from',
        'amount': int(Decimal('10') * Decimal('1e2')),
        'currency': 'USD',
        'other_currency': 'BTC'
    }

    trans = convert_handler._start_trans(**params)
    check_received(trans)
    check_status(trans)
    assert trans['input']['amount'] == params['amount']
    assert trans['input']['currency'] == params['currency']
    assert trans['output']['currency'] == params['other_currency']
    btcusd_rate = Decimal(trans['conversion']['rate'])
    output_amount = int(params['amount'] / Decimal('1e2') / btcusd_rate * Decimal('1e8'))
    assert trans['output']['amount'] == output_amount
    assert trans['output']['amount'] / 1e8 < 0.5


def test_convert_USD_known_BTC():
    # Convert x USD to 0.5 BTC.
    params = {
        'uid': '123', 'address': 'abc',
        'side': 'to',
        'amount': int(Decimal('0.5') * Decimal('1e8')),
        'currency': 'BTC',
        'other_currency': 'USD'
    }

    trans = convert_handler._start_trans(**params)
    check_received(trans)
    check_status(trans)
    assert trans['output']['amount'] == params['amount']
    assert trans['output']['currency'] == params['currency']
    assert trans['input']['currency'] == params['other_currency']
    btcusd_rate = Decimal(trans['conversion']['rate'])
    input_amount = int(params['amount'] / Decimal('1e8') * btcusd_rate * Decimal('1e2'))
    assert trans['input']['amount'] == input_amount
    assert trans['input']['amount'] / 1e2 > 10
