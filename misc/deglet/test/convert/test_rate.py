from deglet.api import convert_handler


def test_rate():
    params = {
        'in_currency': 'BTC',
        'pair': ['BTC', 'USD']
    }

    rate = convert_handler._conversion_rate(**params)
    assert rate > 1


def test_inverse_rate():
    params = {
        'in_currency': 'USD',
        'pair': ['BTC', 'USD']
    }
    rate = convert_handler._conversion_rate(**params)
    assert rate < 1
    assert rate > 0
