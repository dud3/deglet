from ..common import base_server
from . import transaction_handler


def build_app():
    app = base_server.create(transaction_handler)
    app.debug = False
    return app
