"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var cosigner = require('./cosigner');

var app = express();
var port = 3443;


app.use(bodyParser.json());

app.post('/join', function(req, res) {
  var secret = req.body.secret;
  console.log(secret);
  cosigner.join(secret, function(err, info, client) {
    var result = {};

    if (err || info.wallet.status !== 'complete') {
      console.log(err, info);
      return res.json(result);
    }

    return res.json({wallet: client.export()});
  });
});

app.post('/sign', function(req, res) {
  var wallet = req.body.wallet;
  var txid = req.body.txid;
  if (!wallet || !txid) {
    return res.json({fail: 'missing params'});
  }

  cosigner.signAndBroadcast(wallet, txid, function(err, info) {
    if (info && info.ok) {
      return res.json({success: 'ok'});
    }

    console.log(err, info);
    return res.json({fail: err});
  });
});

app.listen(port, function() {
    console.log('Listening to http://localhost:' + port);
});
