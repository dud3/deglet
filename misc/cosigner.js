"use strict";

var BWC = require('bitcore-wallet-client');
var BWS_URL = 'http://deglet.xyz:3232/bws/api';  /* from ServerConstants.js */

var copayerName = 'this-side-cosigner';


function join(secret, callback) {
  var client = new BWC({
    verbose: false,
    baseUrl: BWS_URL
  });

  client.joinWallet(secret, copayerName, function(err, wallet) {
    if (err) {
      callback(err);
      return;
    }
    console.log('Joined ' + wallet.name);
    client.openWallet(function(openErr, info) {
      if (openErr) {
        callback(openErr);
      } else {
        callback(null, info, client);
      }
    });
  });
}


function broadcast(client, txsigned, callback) {
  client.broadcastTxProposal(txsigned, function(bcastErr, res) {
    if (bcastErr) {
      callback(bcastErr);
      return;
    }
    console.log('broadcast', res.txid);
    callback(null, {ok: true});
  });
}


function signAndBroadcast(credentials, txid, callback) {
  var client = new BWC({
    verbose: false,
    baseUrl: BWS_URL
  });

  client.import(credentials);
  console.log(client.credentials.isComplete());

  client.getTx(txid, function(err, tx) {

    if (err) {
      callback(err);
      return;
    }

    //console.log(tx);

    /* 1 signature must be present (this is always 2-of-2 and this cosigner
     * is the last one to sign).
     */
    var m = 2;

    if (tx.actions.length === m) {
      /* Got into a situation where both signatures are present but
       * maybe the transaction didn't get broadcasted. */
      broadcast(client, tx, callback);
      return;
    }

    if (tx.actions.length !== m - 1 ||
        tx.actions[0].type !== 'accept' ||
        tx.actions[0].copayerName === copayerName) {
      callback('need more signatures');
      return;
    }

    client.signTxProposal(tx, function(signErr, txsigned) {
      if (signErr) {
        callback(signErr);
        return;
      }
      //console.log("signTxProposal", txsigned);
      broadcast(client, txsigned, callback);
    });

  });
}


module.exports = {
  join: join,
  signAndBroadcast: signAndBroadcast
};
