import json
import subprocess

from cfg_deglet import BITCOIN_URL


def ping_bitcoind():
    data = json.dumps({'method': 'getinfo'})
    call = ['curl', '--data-binary', '%s' % data, BITCOIN_URL]
    proc = subprocess.Popen(call,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result = proc.communicate()

    assert 'keypoolsize' in result[0], result[0].strip()
    res = json.loads(result[0])
    assert 'connections' in res['result'], res


if __name__ == '__main__':
    ping_bitcoind()
