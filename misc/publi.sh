set -e

BITCOIN="bitcoin-cli"
URL_POST="http://127.0.0.1:6227/transaction/receive"
TXID=$1

RESULT=$($BITCOIN gettransaction $TXID true)

curl -X POST $URL_POST -d "raw=$RESULT"
