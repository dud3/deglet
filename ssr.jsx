/* Server side rendering. */
"use strict";

global.Intl = require('Intl');  // Required by react-intl.
var React = require('react');
var Router = require('react-router');
var LocaleStore = require('./app/src/store/LocaleStore');
var routes = require('./app/routes.jsx');

var TOP = "<!doctype html>" +
"<html>" +
"  <head>" +
"    <meta charset='utf-8'>" +
"    <meta name='viewport' content='initial-scale=1.0'>" +
"    <link href='http://fonts.googleapis.com/css?family=Raleway|Roboto:500,400,300|Roboto+Mono' rel='stylesheet' type='text/css'>" +
"    <link rel='stylesheet' href='/css/app.css'>" +
"    <script src='/js/bwapi.min.js'></script>" +
"    <title>Deglet</title>" +
"  </head>" +
"  <body>";

var BOTTOM = "  </body></html>";


function getLocale(key) {
  var locale = require('./app/locale/messages')[key];
  return locale ? locale : LocaleStore.getLocale();
}

module.exports = function(req, res, lang) {
  var locale = getLocale(lang);
  LocaleStore.setLocale(locale.code);

  var router = Router.create({
    routes: routes,
    location: req.path,
    onAbort: function(abortReason) {
      if (abortReason && abortReason.to) {
        res.redirect(303, abortReason.to);
      }
    },
    onError: function(err) {
      console.log('Error while routing');
      console.log(err);
    }
  });

  router.run(function(Handler) {
    var content = React.renderToString(
      <Handler {...locale} />
    );
    res.send(TOP + content + BOTTOM);
  });
};
