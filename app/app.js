"use strict";

var Router = require('react-router');
var debug = require('debug');

var Constants = require('./src/constant/App');
debug.enable(Constants.log.ns + ':*');
var App = require('./src/component/App.react');
var routes = require('./routes.jsx');

var AppRouter = Router.create({
  routes: routes,
  location: Router.HistoryLocation
});

AppRouter.run(function(Handler) {
  App.CurrentHandler = Handler;
  App.appRender();
});
