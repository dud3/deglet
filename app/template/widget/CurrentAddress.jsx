/*jscs:disable maximumLineLength */

"use strict";

var React = require('react');
var Link = require('react-router').Link;
var FormattedMessage = require('react-intl').FormattedMessage;


function template() {
  return (
    <div className="wallet-address">

      <p className="small-margin">{this.getIntlMessage('widget.current_address.title')}</p>
      <input type="text" title={this.props.address} value={this.props.address}
        onClick={this._onClickSelect} readOnly/>

      <div className="hint" style={{display: this.state.showHint ? "block" : "none"}}>
        <div className="row">
          <div className="col-xs-10">
            <p>
              <FormattedMessage
                message={this.getIntlMessage('widget.current_address.hint')}
                receiveLink={<Link to="receive">{this.getIntlMessage('link.receive')}</Link>} />
            </p>
          </div>
          <div className="col-xs-2 end-xs">
            <a href="#" className="hint-close" title={this.getIntlMessage('generic.hint_close')}
               onClick={this._onHideHint}>
              <span className="icon-cancel"></span>
           </a>
          </div>
        </div>
      </div>

    </div>
  );
}


module.exports = template;
