/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var util = require('util');


function format(data, key, currency) {
  var value = data.change[key].pct;
  var titleFormat = '%s → %s (%s %s %s)';
  var arrow = '';
  var result = {
    color: 'black',
    val: value,
    title: null
  };

  if (value > 0) {
    result.color = 'green';
    result.val = '+' + value;
    arrow = '↑';
  } else if (value < 0) {
    arrow = '↓';
    result.color = 'red';
  }

  result.title = util.format(
    titleFormat,
    data[key], data.last, arrow, data.change[key].amount, currency
  );

  return result;
}


function template() {
  var data = this.state.data;
  var empty = "—";
  var fmt24h = data ? format(data, 'day', this.props.pair[1]) : empty;
  var fmtMonth = data ? format(data, 'month', this.props.pair[1]) : empty;
  var last = data ? data.last : empty;
  var pair = this.props.pair[0] + "/" + this.props.pair[1];

  var content = (
    <div className={"ticker" + (!data ? " ticker-loading" : "")}>
      <table className="ticker-small">
        <thead>
          <tr><th>{pair}</th></tr>
        </thead>
        <tbody>
          <tr><td className="ticker-value">{last}</td></tr>
        </tbody>
      </table>

      <table className="ticker-normal">
        <thead>
          <tr>
            <th colSpan="3">{this.getIntlMessage('widget.ticker.title')} - {pair}</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="ticker-element-title">{this.getIntlMessage('widget.ticker.last')}</td>
            <td className="ticker-element-title">{this.getIntlMessage('widget.ticker.day')}</td>
            <td className="ticker-element-title">{this.getIntlMessage('widget.ticker.month')}</td>
          </tr>
          <tr>
            <td className="ticker-value">{last}</td>
            <td className={"ticker-value ticker-" + fmt24h.color}
                title={data ? fmt24h.title : null}>
              {fmt24h.val}%
            </td>
            <td className={"ticker-value ticker-" + fmtMonth.color}
                title={data ? fmtMonth.title : null}>
              {fmtMonth.val}%
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );

  return content;
}


module.exports = template;
