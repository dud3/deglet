"use strict";

var React = require('react');


function template() {
  var icon;
  var className = "wrapper-dropdown";
  if (this.state.active) {
    className += " active";
    icon = "icon-up-open";
  } else {
    icon = "icon-down-open";
  }

  return (
    <button ref="dropdown" tabIndex="0" onClick={this._toggle} type="button" className={className}>
      <span ref="title">{this.props.title || "Select.."}</span>
      <span ref="right" className={"right " + icon}></span>
      <ul className="dropdown">
        {
          this.props.options.map(function(item) {
            return (
              <li key={"item-" + item.value}>
                <div className="item">
                  <label>
                    <input type="checkbox" value={item.value}/>
                    {item.label}
                  </label>
                </div>
              </li>
            );
          })
        }
      </ul>
    </button>
  );
}


module.exports = template;
