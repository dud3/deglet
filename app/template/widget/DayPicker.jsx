"use strict";

var React = require('react');


function template() {
  return (
    <input
      type="text" ref="pikaday" name={this.props.name}
      onChange={this.props.onManualChange}
      className={this.props.className}
      placeholder={this.props.placeholder}
      disabled={this.props.disabled} />
  );
}


module.exports = template;
