"use strict";

var React = require('react');


function template() {
  var boxType = this.props.error ? "error" : "info";
  var boxMsg = this.props.msg;

  return (
    <div className={boxType + "-box"} style={{display: boxMsg ? "block" : "none"}}>
      {/* Display error or info if there's one. */}
      <div className="box-msg row middle-xs">
        <div className="col-xs-11">
          <h5>{boxMsg}</h5>
        </div>
        <div className="col-xs-1">
          <button onClick={this.props.onHide}>×</button>
        </div>
      </div>
    </div>
  );
}


module.exports = template;
