"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var Modal = require('boron/FadeModal');

var AppLinks = require('../src/constant/AppLinks');
var helper = require('./helper/layout.jsx');


function template() {

  var activeSection = null;
  var txinfoActive = null;
  var horizontalMenu = null;

  function link(item) {
    if (item[activeSection] !== true || item.parent === null || item.sibling) {
      return null;
    }

    return (
      <Link to={item.link} key={item.link + "-" + activeSection}>
        {this.getIntlMessage(item.i18n)}
      </Link>
    );
  }

  if (helper.sectionActive(this, 'wallet')) {
    /* Show a menu for the wallet section. */
    activeSection = 'wallet';
    horizontalMenu = (
      <div>
        <Link to="wallet">{this.getIntlMessage('link.home')}</Link>
        {AppLinks.list.map(link, this)}
      </div>
    );
  } else if (helper.sectionActive(this, 'settings')) {
    /* Show a menu for the settings section. */
    activeSection = 'settings';
    horizontalMenu = (
      <div>
        <Link to="settings">{this.getIntlMessage('link.settings_account')}</Link>
        {AppLinks.list.map(link, this)}
      </div>
    );
  } else {
    /* Show a menu for the txdetail page if it is active. */
    txinfoActive = helper.activeTxDetail(this);
    horizontalMenu = txinfoActive ? (
      <div>
        <Link to="txdetail" params={{txid: txinfoActive || ''}}>
          {this.formatMessage(this.getIntlMessage('widget.txdetail.title'),
                              {id: txinfoActive})}
        </Link>
      </div>
    ) : null;
  }

  return (
    <div>
      <Modal ref="errModal" className="simple-modal">
        <div className="simple-modal-wrapper">
          <h3>{this.getIntlMessage('generic.bad_error')}</h3>
          <p className="tip">{this.state.error && this.getIntlMessage(this.state.error + '.title')}</p>
          <p className="tip-long">{this.state.error && this.getIntlMessage(this.state.error + '.description')}</p>
          <a href="#" onClick={this._onHideError}>{this.getIntlMessage('generic.close')}</a>
        </div>
      </Modal>

      <div className="menucontainer"
           style={{visibility: horizontalMenu ? "visible" : "hidden"}}>
        <div className="horizmenu">
          {horizontalMenu}
        </div>
      </div>

      <Router.RouteHandler {...this.state} />
    </div>
  );
}


module.exports = template;
