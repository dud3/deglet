/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Link = require('react-router').Link;
var LaddaButton = require('react-ladda');

var helper = require('../helper/history.jsx');
var fmtAmount = require('../helper').formatAmount;
var TableFilter = require('../../src/component/TableFilter.react');


function template() {

  var i18n = this.getIntlMessage;
  var hist = this.state.store.history || [];

  /* Make sure the results visible are only for this page. */
  var store = this.state.store;
  var firstPage = store.pagesAvail[0];
  hist = hist.slice(
    (store.currPage - firstPage) * store.resultsPerPage,
    (store.currPage - firstPage + 1) * store.resultsPerPage
  );

  function histFormat(entry) {
    var key = entry.txid + "-hist";
    return (
      <tr key={key}>
        <td className="txid" title={entry.txid}>
          <Link to="txdetail" params={{txid: entry.txid}}>{entry.txid}</Link>
        </td>
        <td data-label={i18n('history.table.when')}>{this.formatDate(entry.time * 1000, 'shortWithTime')}</td>
        <td data-label={i18n('history.table.type')}>{i18n('transaction.type.' + entry.action)}</td>
        <td data-label={i18n('generic.amount')}>{fmtAmount(entry.amount, 'BTC')}</td>
        <td className="hide-small">{fmtAmount(entry.fees, 'BTC')}</td>
        <td className="hide-small">{this.formatNumber(entry.confirmations || 0)}</td>
      </tr>
    );
  }

  return (
    <div className="user-page">

      <div className="row info-row">
        {helper.status(this, hist)}
        {helper.filterButtons(this, hist.length, this._toggleFilter, this._clearFilters)}
      </div>
      <TableFilter
        ref="tablefilter" titleId="history.filter_by.id_or_destaddress"
        typeFilter={true} dateFilter={true}
        show={this.state.showFilter}
        filtering={this.state.loading}
        onFilter={this._applyFilter}/>

      <table className="user-table">

        <thead>
          <tr>
            <th>TXID</th>
            <th>
              <a onClick={this._onTableSort.bind(this, "when")}>
                {i18n('history.table.when')} <span className={this._tableIcon("when")}></span>
              </a>
            </th>
            <th>{i18n('history.table.type')}</th>
            <th>
              <a onClick={this._onTableSort.bind(this, "amount")}>
                {i18n('generic.amount')} <span className={this._tableIcon("amount")}></span>
              </a>
            </th>
            <th className="hide-small">
              <a onClick={this._onTableSort.bind(this, "fees")}>
                {i18n('generic.fees')} <span className={this._tableIcon("fees")}></span>
              </a>
            </th>
            <th className="hide-small">{i18n('generic.confs')}</th>
          </tr>
        </thead>

        <tbody className={this.state.loading ? "loading" : null}>
          {hist.map(histFormat, this)}
        </tbody>

      </table>

      <div className="row middle-xs table-footer">

        <div className="col-xs-8 pagination">
          {helper.paginate(
            this.state.store.currPage,
            this.state.store.pageCount,
            this._onPaginate)}
        </div>

        <div className="col-xs-4 end-xs">
          {hist.length ?
            <LaddaButton className="button action-button export-label" onClick={this._onExport}
                         data-label={i18n('history.button.export')}
                         data-small-label={i18n('history.button.export_small')}
                         buttonStyle="zoom-in" loading={this.state.exporting}>
            </LaddaButton> : null}
        </div>
        <a ref="export" style={{display: "none"}} download></a>

      </div>

    </div>
  );

}


module.exports = template;
