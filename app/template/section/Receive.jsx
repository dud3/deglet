/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;


function template() {
  return (
    <div>

      <div className="user-tabs">
        <Link to="recvsimple">{this.getIntlMessage('link.recvsimple')}</Link>
        <Link to="recvinvoice">{this.getIntlMessage('link.recvinvoice')}</Link>
      </div>
      <hr className="hr-tabs"/>

      <Router.RouteHandler {...this.props} />

    </div>
  );
}


module.exports = template;
