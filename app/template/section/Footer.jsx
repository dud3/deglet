"use strict";

var React = require('react');
var LocaleStore = require('../../src/store/LocaleStore');


function template() {
  var lang = LocaleStore.localeOrder();
  var currentLocale = LocaleStore.getLocale().code;
  var loggedin = this.props.username;

  var langLinks = lang.map(function(entry) {
    var title = this.getIntlMessage('lang.' + entry.code + '_name');
    return (<a href="#" key={entry.code}
               className={currentLocale === entry.code ? "active" : ""}
               onClick={this._setLocale.bind(this, entry.code)}
               title={title}>{entry.name}</a>);
  }, this);

  return (
    <footer>
    <div className={"footer" + (loggedin ? " user" : "")}>
      <div className="lang">
        {langLinks}
      </div>

      <div>
        <hr/>
        <a href="https://bitbucket.org/deginner/deglet" target="_blank">
          {this.getIntlMessage('generic.source_code')}
        </a>
      </div>
    </div>
   </footer>
  );
}

module.exports = template;
