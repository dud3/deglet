/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Link = require('react-router').Link;
var FormattedMessage = require('react-intl').FormattedMessage;
var LaddaButton = require('react-ladda');
var pwdMinlen = require('../../src/constant/App').pwdMinlen;
var BoxMessage = require('../../src/component/widget/BoxMessage.react');


function template() {
  var pattern = ".{" + pwdMinlen + ",}";
  var pwdTitle = this.formatMessage(this.getIntlMessage('login.pwdminlen'),
                                    {num: pwdMinlen});

  return (
    <form onSubmit={this._submit} onChange={this._update} autoCorrect="off" autoCapitalize="none">
      <div className="row">
        <div className="col-xs-12">
          <input placeholder={this.getIntlMessage('login.username')}
                 name="username" type="text" ref="username" required
                 className="username"/>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12">
          <input placeholder={this.formatMessage(this.getIntlMessage('login.password_new'),
                                                 {num: pwdMinlen})}
                 pattern={pattern} title={pwdTitle}
                 name="password" type="password" autoComplete="off" required
                 className="password"/>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12">
          <input placeholder={this.formatMessage(this.getIntlMessage('signup.passwordrepeat'),
                                                 {num: pwdMinlen})}
                 pattern={pattern} title={pwdTitle}
                 name="passwordrepeat" type="password" autoComplete="off" required
                 className="password"/>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-7">
          <LaddaButton buttonStyle="zoom-in" loading={this.state.waiting} type="submit"
                       className="button button-full main-button text-heavy">
            {this.getIntlMessage('signup.button')}
          </LaddaButton>
        </div>
      </div>
      <div className="separator-m" />
      <div className="row">
        <div className="col-xs-12">
          <h5>{this.getIntlMessage('signup.tosagree')}</h5>
        </div>
      </div>

      <BoxMessage
        error={this.state.error ? true : false}
        msg={this.formatMessage(this.getIntlMessage(this.state.info), {num: pwdMinlen})}
        rawMsg={this.getIntlMessage(this.state.info)}
        onHide={this._onHideMessage}
      />

      <div className="login-signup"
           style={{display: (this.state.waiting || this.state.info) ? "none" : "block"}}>
        <FormattedMessage
          message={this.getIntlMessage('login.existinguser')}
          linkLogin={<Link to="login">{this.getIntlMessage('link.login_long')}</Link>}
        />
      </div>
    </form>
  );
}


module.exports = template;
