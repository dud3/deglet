/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;


function template() {
  return (
    <div>

      <div className="user-tabs">
        <Link to="histbitcoin">{this.getIntlMessage('link.histbitcoin')}</Link>
        <Link to="histexternal">{this.getIntlMessage('link.histexternal')}</Link>
      </div>
      <hr className="hr-tabs"/>

      <Router.RouteHandler client={this.props.wallet.client} />

    </div>
  );
}


module.exports = template;
