/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Link = require('react-router').Link;
var LaddaButton = require('react-ladda');
var FormattedMessage = require('react-intl').FormattedMessage;
var BoxMessage = require('../../src/component/widget/BoxMessage.react');
var helper = require('../helper/layout.jsx');


var currencies = ['BTC', 'USD'];


function template() {

  var infoMsg = null;
  var rawMsg = null;

  if (this.state.err) {
    rawMsg = this.getIntlMessage(this.state.err);
    infoMsg = rawMsg;
  } else if (this.state.info) {
    rawMsg = this.getIntlMessage('receive.invoice.created');
    infoMsg = (
      <FormattedMessage
        message={rawMsg}
        id={<Link to="invoice" params={{id: this.state.info}} target="_blank">{this.state.info}</Link>}
      />
    );
  }

  return (
      <div className="row">
        <div className="col-xs-12 col-sm-5 col-md-4">

          <form onSubmit={this._onSubmit}>
            {helper.rowAddon(
              this.getIntlMessage('receive.invoice.label'),
              <div className="select-dropdown right-addon button">
                <select ref="currency">
                  {currencies.map(function(val) {
                    return <option key={"receive-" + val} value={val}>{val}</option>;
                  })}
                </select>
              </div>,
              <input type="number" ref="amount" placeholder={this.getIntlMessage('generic.amount')}
                     className="has-right-addon"
                     min="0.00000001" step="any" required/>)
            }

            {/* Options. */}
            <fieldset style={{marginTop: '2rem', borderLeft: 0, borderRight: 0, padding: 0}}>
              <legend style={{borderBottom: '1px solid gray', width: '100%', paddingBottom: '0.5rem'}}>
                <a href="#" onClick={this._onToggleOptions}>
                  {this.state.showOptions ?
                    this.getIntlMessage('receive.invoice.option.hide') :
                    this.getIntlMessage('receive.invoice.option.show')}
                </a>
              </legend>
              <div style={{padding: '1rem 0', display: this.state.showOptions ? "block" : "none"}}>
                {helper.row(
                  this.getIntlMessage('receive.invoice.option.description'), null,
                  <input type="text" ref="title" maxLength={this.state.titleMaxLen}
                         placeholder={this.formatMessage(
                           this.getIntlMessage('receive.invoice.option.description_len'),
                           {num: this.state.titleMaxLen})} />
                )}
              </div>
            </fieldset>

            {/* Submit button. */}
            {helper.row(
              null, "col-xs-7",
              <LaddaButton buttonStyle="zoom-in" loading={this.state.submitted}
                           type="submit" className="button button-full action-button text-heavy">
                {this.getIntlMessage('receive.invoice.button')}
              </LaddaButton>)}
            </form>

            <BoxMessage
              error={this.state.err ? true : false}
              msg={infoMsg} rawMsg={rawMsg}
              onHide={this._hideMessage} />

        </div>

      </div>
  );
}


module.exports = template;
