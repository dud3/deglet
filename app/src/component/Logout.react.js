"use strict";

var React = require('react');
var Router = require('react-router');

var LoginAction = require('../action/LoginAction');
var debug = require('../debug')(__filename);


var Logout = React.createClass({

  mixins: [Router.Navigation],

  componentDidMount: function() {
    debug('Logout mounted');
    LoginAction.logout();

    var self = this;
    setTimeout(function() {
      self.transitionTo('login');
    }, 50);
  },

  render: function() {
    return null;
  }

});


module.exports = Logout;
