"use strict";

var React = require('react');
var Router = require('react-router');
var ReactIntl = require('react-intl');
var template = require('../../../template/widget/SlideMenu.jsx');


var SlideMenu = React.createClass({

  mixins: [ReactIntl.IntlMixin, Router.State],

  propTypes: {
    showButton: React.PropTypes.bool.isRequired
  },

  getInitialState: function() {
    return {
      show: false
    };
  },

  componentDidMount: function() {
    window.addEventListener('resize', this._onResize);
  },

  componentWillUnmount: function() {
    window.removeEventListener('resize', this._onResize);
  },

  componentWillReceiveProps: function(nextProps) {
    if (!nextProps.showButton) {
      this.closeMenu();
    }
  },

  _onResize: function() {
    /* Make sure the mobile menu is not present after
     * a window resize. */
    this.closeMenu();
  },

  openMenu: function() {
    this.setState({show: true}, function() {
      document.body.classList.add('slideout-open');
    });
  },

  closeMenu: function(event) {
    if (event) {
      event.preventDefault();
    }

    this.setState({show: false}, function() {
      document.body.classList.remove('slideout-open');
    });
  },

  toggleMenu: function() {
    var show = this.state.show;
    if (show) {
      /* Menu is currently open, close it. */
      this.closeMenu();
    } else {
      /* Menu is closed, open it. */
      this.openMenu();
    }
  },

  render: template

});


module.exports = SlideMenu;
