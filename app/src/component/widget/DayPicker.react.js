"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var canUseDOM = require('react/lib/ExecutionEnvironment').canUseDOM;
var Pikaday = canUseDOM ? require('pikaday') : function() {};

var LocaleStore = require('../../store/LocaleStore');
var template = require('../../../template/widget/DayPicker.jsx');


var DayPicker = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  propTypes: {
    maxDate: React.PropTypes.instanceOf(Date),
    value: React.PropTypes.instanceOf(Date),
    onChange: React.PropTypes.func
  },

  setDateIfChanged: function(newDate, prevDate) {
    var newTime = newDate ? newDate.getTime() : null;
    var prevTime = prevDate ? prevDate.getTime() : null;

    if (newTime !== prevTime) {
      if (newDate === null) {
        // Workaround for pikaday not clearing value when date set to falsey
        React.findDOMNode(this.refs.pikaday).value = '';
      }
      this._picker.setDate(newDate, true); // 2nd param = don't call onSelect
    }
  },

  componentDidMount: function() {
    var el = React.findDOMNode(this.refs.pikaday);
    this._picker = new Pikaday({
      field: el,
      onSelect: this.props.onChange,
      maxDate: this.props.maxDate,
      i18n: this.getIntlMessage('calendar')
    });
    this.setDateIfChanged(this.props.value);

    LocaleStore.addChangeListener(this._onLocaleChange);
  },

  componentWillReceiveProps: function(nextProps) {
    var newDate = nextProps.value;
    var lastDate = this.props.value;
    this.setDateIfChanged(newDate, lastDate);
  },

  componentWillUnmount: function() {
    LocaleStore.removeChangeListener(this._onLocaleChange);
  },

  _onLocaleChange: function() {
    var newCalendar = LocaleStore.getLocale().messages.calendar;
    this._picker.config({i18n: newCalendar});
  },

  destroy: function() {
    this._picker.destroy();
  },

  render: template

});


module.exports = DayPicker;
