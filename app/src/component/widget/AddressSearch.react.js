"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var WalletAction = require('../../action/WalletAction');
var AddressInfoStore = require('../../store/AddressInfoStore');
var template = require('../../../template/widget/AddressSearch.jsx');


var AddressSearch = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  propTypes: {
    client: React.PropTypes.object
  },

  getInitialState: function() {
    return {
      address: null,
      loading: false,
      error: false,
      info: ''
    };
  },

  componentDidMount: function() {
    AddressInfoStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    AddressInfoStore.removeChangeListener(this._onChange);
  },

  _onChange: function() {
    var key;
    var error;
    var data = AddressInfoStore.getData();

    if (data.error) {
      key = data.error;
      error = true;
    } else {
      if (data.walletId) {
        key = 'widget.address.is_mine';
        error = false;
      } else if (data.uid) {
        key = 'widget.address.is_partially_mine';
        error = false;
      } else {
        key = 'widget.address.is_not_mine';
        error = true;
      }
    }

    this.setState({loading: false, error: error, info: key});
  },

  _onSubmit: function(event) {
    event.preventDefault();

    var address = React.findDOMNode(this.refs.address).value.trim();
    var client = this.props.client;

    if (!client) {
      this.setState({error: true, info: 'error.no_wallet'});
      return;
    } else if (!address) {
      this.setState({error: true, info: 'error.invalid_address'});
      return;
    }

    this.setState(
      {loading: true, address: address, error: false, info: ''},
      WalletAction.fetchAddress(client, address)
    );
  },

  _onHideMessage: function(event) {
    event.preventDefault();
    this.setState({error: false, info: ''});
  },

  render: template

});


module.exports = AddressSearch;
