"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var BalanceStore = require('../../store/BalanceStore');
var template = require('../../../template/widget/Balance.jsx');

var permState = {show: true};


var Balance = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  getInitialState: function() {
    return {
      show: permState.show,
      balance: BalanceStore.getData()
    };
  },

  componentDidMount: function() {
      BalanceStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
      BalanceStore.removeChangeListener(this._onChange);
  },

  _onChange: function() {
    this.setState({balance: BalanceStore.getData()});
  },

  onToggle: function() {
    permState.show = !permState.show;
    this.setState({show: permState.show});
  },

  render: template

});


module.exports = Balance;
