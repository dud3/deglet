"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var template = require('../../../template/widget/CurrentAddress.jsx');

var permShowHint = true;


var CurrentAddress = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  propTypes: {
    address: React.PropTypes.string.isRequired
  },

  getInitialState: function() {
    return {
      showHint: permShowHint
    };
  },

  _onClickSelect: function(event) {
    event.target.select();
  },

  _onHideHint: function() {
    permShowHint = !permShowHint;
    this.setState({showHint: permShowHint});
  },

  render: template

});


module.exports = CurrentAddress;
