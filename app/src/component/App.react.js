"use strict";

var React = require('react');
var Router = require('react-router');
var ReactIntl = require('react-intl');

var LoginStore = require('../store/LoginStore');
var LocaleStore = require('../store/LocaleStore');
var template = require('../../template/App.jsx');
var debug = require('../debug')(__filename);


var WalletApp = React.createClass({

  mixins: [ReactIntl.IntlMixin, Router.State],

  getInitialState: function() {
    return LoginStore.getData();
  },

  componentDidMount: function() {
    debug('mounted');
    LoginStore.addChangeListener(this._onChange);
    LocaleStore.addChangeListener(this._onLocaleChange);
  },

  componentWillUnmount: function() {
    debug('unmounting');
    LoginStore.removeChangeListener(this._onChange);
    LocaleStore.removeChangeListener(this._onLocaleChange);
  },

  componentWillUpdate: function() {
    /* Make sure the mobile menu is always closed
     * when moving to a new page. */
    this.refs.mobileMenu.closeMenu();
  },

  _onChange: function() {
    this.setState(LoginStore.getData());
  },

  _onLocaleChange: function() {
    WalletApp.appRender();
  },

  render: template

});


WalletApp.CurrentHandler = null;
WalletApp.appRender = function() {
  /* Re-render the app. This is used to switch locales. */
  debug('appRender');
  if (WalletApp.CurrentHandler) {
    React.render(<WalletApp.CurrentHandler {...LocaleStore.getLocale()} />,
                 document.body);
  }
};


module.exports = WalletApp;
