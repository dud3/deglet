"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var template = require('../../../template/section/NotFound.jsx');


var NotFound = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  render: template

});


module.exports = NotFound;
