"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var WalletStore = require('../../store/WalletStore');
var SettingsStore = require('../../store/SettingsStore');
var template = require('../../../template/section/Settings.jsx');


var Settings = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  getInitialState: function() {
    return {
      settings: SettingsStore.getData(),
      wallet: WalletStore.getData()
    };
  },

  componentDidMount: function() {
    WalletStore.addChangeListener(this._onWalletChange);
    SettingsStore.addChangeListener(this._onSettingsChange);
  },

  componentWillUnmount: function() {
    WalletStore.removeChangeListener(this._onWalletChange);
    SettingsStore.removeChangeListener(this._onSettingsChange);
  },

  _onWalletChange: function() {
    this.setState({wallet: WalletStore.getData()});
  },

  _onSettingsChange: function() {
    this.setState({settings: SettingsStore.getData()});
  },

  render: template

});


module.exports = Settings;
