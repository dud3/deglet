"use strict";

var React = require('react');
var Router = require('react-router');
var ReactIntl = require('react-intl');

var LoginStore = require('../../store/LoginStore');
var template = require('../../../template/section/Wallet.jsx');
var thisServer = require('../../constant/ServerConstants').THIS_URL;


var Wallet = React.createClass({

  mixins: [ReactIntl.IntlMixin, Router.Navigation],

  getInitialState: function() {
    return LoginStore.getData();
  },

  _registerProtoAvail: function() {
    return (typeof window.navigator.registerProtocolHandler === "function");
  },

  _registerBitcoin: function() {
    var scheme = 'bitcoin';
    var url = thisServer + this.makeHref('send', null, {uri: ''}) + '%s';
    var title = 'Deglet';

    window.navigator.registerProtocolHandler(scheme, url, title);
  },

  _hideNewUser: function(event) {
    event.preventDefault();
    LoginStore.noNewUser();
    this.setState({newuser: false});
  },

  _showTip: function(event) {
    event.preventDefault();
    this.refs.tipModal.show();
  },

  _hideTip: function(event) {
    event.preventDefault();
    this.refs.tipModal.hide();
  },

  render: template

});


module.exports = Wallet;
