"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var template = require('../../../template/section/Receive.jsx');


var Receive = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  render: template

});


module.exports = Receive;
