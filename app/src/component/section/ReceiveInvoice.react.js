"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var WalletAction = require('../../action/WalletAction');
var InvoiceAction = require('../../action/InvoiceAction');
var InvoiceStore = require('../../store/InvoiceStore');
var LoginStore = require('../../store/LoginStore');
var template = require('../../../template/section/ReceiveInvoice.jsx');


var ReceiveInvoice = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  getInitialState: function() {
    return {
      titleMaxLen: 22,
      showOptions: false,
      submitted: false,
      err: '',
      info: ''
    };
  },

  componentDidMount: function() {
    InvoiceStore.addChangeListener(this._onInvoiceChange);

    var width = window.innerWidth || 1000;
    if (width >= 768) {
      React.findDOMNode(this.refs.amount).focus();
    }
  },

  componentWillUnmount: function() {
    InvoiceStore.clear();
    InvoiceStore.removeChangeListener(this._onInvoiceChange);
  },

  _onToggleOptions: function(event) {
    event.preventDefault();

    var show = this.state.showOptions;
    this.setState({showOptions: !show});
  },

  _onInvoiceChange: function() {
    var data = InvoiceStore.getData();

    /* Update receiving address. */
    var client = this.props.wallet.client;
    WalletAction.newAddress(client);

    this.setState({
      submitted: false,
      err: data.error,
      info: data.error ? null : data.obj.id
    });
  },

  _hideMessage: function() {
    this.setState({err: '', info: ''});
  },

  _onSubmit: function(event) {
    event.preventDefault();
    if (this.state.submitted) {
      /* Waiting for result. */
      return;
    }

    var data = {
      uid: LoginStore.getData().uid,
      address: this.props.wallet.info.address.address,
      amount: React.findDOMNode(this.refs.amount).value,
      currency: React.findDOMNode(this.refs.currency).value
    };
    var title = React.findDOMNode(this.refs.title).value;
    if (title) {
      data.title = title;
    }

    this.setState({submitted: true, err: '', info: ''}, function() {
      InvoiceAction.create(data);
    });
  },

  render: template

});


module.exports = ReceiveInvoice;
