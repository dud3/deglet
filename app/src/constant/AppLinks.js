var linkName = {
  index: 'welcome',
  wallet: 'wallet',
  txinfo: 'tx'
};

var linkList = [
  {
    link: linkName.wallet,
    i18n: "link.wallet",
    wallet: true,   /* Page exclusive to wallet. */
    parent: null    /* Top level link. */
  },
  {
    link: "send",
    i18n: "link.send",
    wallet: true
  },
  {
    link: "receive",
    i18n: "link.receive",
    wallet: true
  },
  {
    link: "convert",
    i18n: "link.convert",
    wallet: true
  },
  {
    link: "history",
    i18n: "link.history",
    wallet: true
  },

  {
    link: "receive/recvinvoice",
    i18n: "link.recvinvoice",
    wallet: true,
    sibling: "receive"
  },
  {
    link: "receive/recvsimple",
    i18n: "link.recvsimple",
    wallet: true,
    sibling: "receive"
  },
  {
    link: 'history/histexternal',
    i18n: 'link.histexternal',
    wallet: true,
    sibling: 'history'
  },
  {
    link: 'history/histbitcoin',
    i18n: 'link.histbitcoin',
    wallet: true,
    sibling: 'history'
  },

  {
    link: "settings",
    i18n: "link.settings",
    parent: null,
    settings: true
  },

  {
    link: "logout",
    i18n: "link.logout",
    parent: null,
    separate: true
  }
];

var linkIndex = {};
for (var i = 0; i < linkList.length; i++) {
  var item = linkList[i];
  linkIndex[item.link] = i;
}

module.exports = {
  list: linkList,
  linkIndex: linkIndex,
  name: linkName
};
