var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
  CHANGE_EVENT: null,
  REFRESH_EVENT: null,
  DOWNLOAD_EVENT: null
});
