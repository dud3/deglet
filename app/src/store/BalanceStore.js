"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');

var _balance;
var _order = ['BTC', 'USD'];


function _initialBalance() {
  return new Array(_order.length);
}


function _logout() {
  _balance = _initialBalance();
}


function _updateBitcoinBalance(data) {
  var currency = 'BTC';
  _balance[_order.indexOf(currency)] = {
    total: data.totalAmount,
    locked: data.lockedAmount,
    currency: currency
  };
}


function _updateRawBalance(amount, currency) {
  _balance[_order.indexOf(currency)] = {
    total: amount,
    locked: 0,
    currency: currency
  };
}


_balance = _initialBalance();


var BalanceStore = BaseStore.create();


BalanceStore.getData = function() {
  return _balance;
};


BalanceStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.LOGIN_OK: {
      _updateRawBalance(action.usdbalance, 'USD');
      BalanceStore.emitChange();
      break;
    }

    case ActionTypes.RAW_FIAT_BALANCE: {
      _updateRawBalance(action.data.usdbalance, 'USD');
      BalanceStore.emitChange();
      break;
    }

    case ActionTypes.FIAT_BALANCE: {
      _updateRawBalance(action.data.total, action.data.currency);
      BalanceStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_BALANCE: {
      _updateBitcoinBalance(action.data);
      BalanceStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_STATUS: {
      _updateBitcoinBalance(action.data.balance);
      BalanceStore.emitChange();
      break;
    }

    case ActionTypes.LOGOUT: {
      _logout();
      BalanceStore.emitChange();
      break;
    }

  }
});


module.exports = BalanceStore;
