"use strict";

var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var AppDispatcher = require('../dispatcher');
var EventConstants = require('../constant/EventConstants');
var storage = require('./storage');
var tempstore = require('./tempstore');
var debug = require('../debug')(__filename);

var _settingsInfo;


function _initialSettingsInfo() {
  return {
    pgpkey: '',  /* Public PGP key for encrypting email. */
    email: '',   /* Email address for communication. */
    status: {    /* Updated during the submit process. */
      info: '',
      error: false
    }
  };
}

/* Load settings info stored previously. */
_settingsInfo = assign({}, _initialSettingsInfo());


var SettingsStore = assign({}, EventEmitter.prototype, {
  getData: function() {
    return _settingsInfo;
  },

  emitChange: function() {
    this.emit(EventConstants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(EventConstants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(EventConstants.CHANGE_EVENT, callback);
  }
});


SettingsStore.dispatchToken = AppDispatcher.register(function(action) {

  switch (action.actionType) {

    /* No actions handled yet. */

  }

});


module.exports = SettingsStore;
