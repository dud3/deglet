"use strict";

var store = require('store2');


var _key = {
  locale: 'user.cfg',
  login: 'user.account',
  wallet: 'user.wallet',
  walletInfo: 'user.wallet.info'
};


function _storeValue(key, value) {
  if (store.isFake()) {
    /* XXX Display error to the user to indicate that
     * the local storage is not available. Safari disables
     * it when using private browsing.
     */
    console.log('local storage disabled!!', key);
    return;
  }
  store.set(key, value);
}


var locale = {
  key: _key.locale,

  save: function(key) {
    _storeValue(this.key, {lang: key});
    if (typeof document !== 'undefined') {
      document.cookie = 'lang=' + key + ';path=/';
    }
  },

  get: function() {
    return store.get(this.key) || {};
  }
};


var login = {
  key: _key.login,

  save: function(username, blob) {
    _storeValue(this.key, {username: username, blob: blob});
  },

  get: function() {
    var data = store.get(this.key) || {};
    if (typeof data !== "object") {
      /* XXX Maybe give a chance to the user grab this data before
       * erasing it locally.
       */
      console.log("bad data stored in", this.key, "erasing..");
      this.erase();
      data = {};
    }
    return data;
  },

  erase: function() {
    store.remove(this.key);
  }
};


var wallet = {
  key: _key.wallet,

  save: function(data) {
    _storeValue(this.key, data);
  },

  get: function() {
    return store.get(this.key) || null;
  },

  erase: function() {
    store.remove(this.key);
  }
};


var walletInfo = {
  key: _key.walletInfo,

  initial: function() {
    return {
      balance: null,
      address: null,   /* Last address created. XXX not kept in sync yet */
      error: null
    };
  },

  save: function(walletId, data) {
    _storeValue(this.key + '.' + walletId, data);
  },

  get: function(walletId) {
    return store.get(this.key + '.' + walletId) || this.initial();
  },

  erase: function(walletId) {
    store.remove(this.key + '.' + walletId);
  }
};


module.exports = {
  locale: locale,
  login: login,
  wallet: wallet,
  walletInfo: walletInfo
};
