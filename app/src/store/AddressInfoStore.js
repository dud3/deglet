"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');
var request = require('../util/request');
var errorkey = require('../util/errorkey');

var _info = null;


var AddressInfoStore = BaseStore.create();


AddressInfoStore.getData = function() {
  return _info;
};


AddressInfoStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.WALLET_ADDRESS_INFO: {
      _info = action.data;
      AddressInfoStore.emitChange();
      break;
    }

    case ActionTypes.ADDRESS_EXTERNAL_INFO: {
      var address = action.data.address;
      delete action.data.address;

      request.get(
        '/address/' + address, action.data,
        function(data) {
          _info = data;
          AddressInfoStore.emitChange();
        },
        function(err, info) {
          console.log('/address failed', err, info);
          var key = 'error.unknown';
          _info = {error: key};
          AddressInfoStore.emitChange();
        }
      );
      break;
    }

    case ActionTypes.WALLET_ERROR: {
      if (action.error.name === ActionTypes.WALLET_ADDRESS_INFO) {
        _info = {error: errorkey.bws(action.error.reason)};
        AddressInfoStore.emitChange();
      }
      break;
    }

  }

});


module.exports = AddressInfoStore;
