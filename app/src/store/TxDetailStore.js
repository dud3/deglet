"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');

var _tx = null;


var TxDetailStore = BaseStore.create();


TxDetailStore.getData = function() {
  return _tx;
};


TxDetailStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.TXINFO: {
      _tx = action.data;
      TxDetailStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_ERROR: {
      if (action.error.name === ActionTypes.TXINFO) {
        _tx = {error: 'error.blockchain.tx'};
        TxDetailStore.emitChange();
      }
      break;
    }

  }

});


module.exports = TxDetailStore;
