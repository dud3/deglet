"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');
var debug = require('../debug')(__filename);


var _info = {
  error: null
};


var WalletCosignStore = BaseStore.create();


WalletCosignStore.getData = function() {
  return _info;
};


WalletCosignStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.COSIGN_OK: {
      debug('accepted tx', action);
      _info.error = null;
      WalletCosignStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_ERROR: {
      if (action.error.name === ActionTypes.COSIGN_FAIL) {
        debug('rejected tx', action);
        _info.error = action.error.reason;
        WalletCosignStore.emitChange();
      }
      break;
    }

  }

});


module.exports = WalletCosignStore;
