"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');
var History = require('./History');
var request = require('../util/request');
var debug = require('../debug')(__filename);

var _sortableColumns = [];
var _filterableColumns = ['id', 'time', 'payment_address'];
var _history = new History.History(_sortableColumns, _filterableColumns);

_history.historyData = _history.initialHistoryData();


var HistoryExternalStore = BaseStore.create();


HistoryExternalStore.getData = function() {
  return _history.historyData;
};


HistoryExternalStore.sort = function(column) {
  var didSort = _history.localSort(column);
  if (didSort) {
    this.emitChange();
  }
  return didSort;
};


HistoryExternalStore.filter = function(match) {
  var didFilter = _history.localFilter(match);
  if (didFilter) {
    this.emitChange();
  }
  return didFilter;
};


HistoryExternalStore.clearFilters = function() {
  var allClean = _history.clearFilters();
  if (allClean) {
    this.emitChange();
  }
  return allClean;
};


HistoryExternalStore.setCurrPage = function(pageNum) {
  var changed = _history.setCurrPage(pageNum);
  if (changed) {
    this.emitChange();
  }
  return changed;
};


function _adjustData(data) {
  var entry;
  for (var i = 0; i < data.length; i++) {
    entry = data[i];
    entry.time = entry.status_info.slice(-1)[0].date.epoch_time;
  }
}


HistoryExternalStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.HISTORY_EXTERNAL_FETCH: {
      var opts;
      var firstPage = action.data.firstPage;
      delete action.data.firstPage;

      opts = History.prepareQuery(_history, firstPage, false);
      /* Filter and sort need to be converted manually. */
      /* XXX Missing code for sort. */
      for (var i = 0; i < opts.filter.length; i++) {
        if (opts.filter[i].name === 'time') {
          opts.from = opts.filter[i].query.from;
          opts.to = opts.filter[i].query.to;
        } else {
          opts[opts.filter[i].name] = opts.filter[i].query;
        }
      }
      delete opts.filter;
      delete opts.sort;
      action.data.query = opts;

      request.get(
        '/history/' + firstPage, action.data,
        function(data) {
          /* Got data. */
          data.firstPage = firstPage;
          _adjustData(data.data);
          _history.historyData.error = '';
          _history.updateRawData(data);
          HistoryExternalStore.emitChange();
        },
        function(err, info) {
          debug('/history failed', err, info);
          var key = BaseStore.parseDegletError(info);
          if (key === null) {
            key = 'error.blockchain.history';
          }
          _history.historyData.error = key;
          HistoryExternalStore.emitChange();
        }
      );
      break;
    }

    case ActionTypes.LOGOUT: {
      /* Cleanup. */
      _history.historyData = _history.initialHistoryData();
      break;
    }

  }

});


module.exports = HistoryExternalStore;
