"use strict";

var assign = require('object-assign');
var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');
var tempstore = require('./tempstore');
var storage = require('./storage');
var debug = require('../debug')(__filename);

var _loginInfo;


function _initialLoginInfo() {
  return {
    username: '', /* Username */
    key: '',      /* Encryption key; generated and maintained locally only. */
    uid: '',      /* UID derived from key; sent to the server. */
    blob: null,   /* Encrypted blob stored in the server. */
    status: {     /* Updated during the submit process. */
      info: '',
      error: false
    },
    newuser: false  /* Was this account just created? */
  };
}

/* Load login info stored previously. */
_loginInfo = assign({}, _initialLoginInfo(),
                    storage.login.get(),
                    tempstore.get());


function update(obj) {
  if (obj.username) {
    /* User is now logged in, store data locally. */
    storage.login.save(obj.username, obj.blob);
  }

  if (obj.key || obj.uid) {
    tempstore.save({uid: obj.uid, key: obj.key});
  }

  /* Merge objects. */
  _loginInfo = assign(_loginInfo, obj);
}


function logout() {
  storage.login.erase();
  tempstore.erase();
  _loginInfo = _initialLoginInfo();
}


var LoginStore = BaseStore.create();


LoginStore.getData = function() {
  return _loginInfo;
};


LoginStore.noNewUser = function() {
  _loginInfo.newuser = false;
};


LoginStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.LOGIN_OK: {
      debug('server accepted login');
      update({
        username: action.username,
        key: action.key,
        uid: action.uid,
        blob: action.blob,
        status: {
          info: '',
          error: false
        },
        newuser: action.newuser ? true : false
      });

      /* When the server receives the following cookie it
       * never renders the page before sending to this client.
       */
      document.cookie = "user=1; path=/";

      LoginStore.emitChange();
      break;
    }

    case ActionTypes.LOGIN_STATUS: {
      update({
        status: {
          info: action.info,
          error: action.error === true
        }
      });
      LoginStore.emitChange();
      break;
    }

    case ActionTypes.LOGOUT: {
      debug('received action logout');
      logout();
      document.cookie = "user=0;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/";
      LoginStore.emitChange();
      break;
    }

    case ActionTypes.BLOB_STORE: {
      debug('encrypted blob received, store locally.');
      update({username: _loginInfo.username, blob: action.blob});
      /* No change event needs to be emitted here, as this action
       * does not require any UI updates. */
      break;
    }

  }

});


module.exports = LoginStore;
