"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');
var request = require('../util/request');
var debug = require('../debug')(__filename);


var _info = {
  error: true,
  result: null
};


var PayExternalStore = BaseStore.create();


PayExternalStore.getData = function() {
  return _info;
};


PayExternalStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.CONVERSION_EXTERNAL_PAY: {
      debug('pay conversion', action);
      var id = action.data.transId;
      delete action.data.transId;

      request.post(
        '/conversion/' + id, action.data,
        function (data) {
          if (data.error) {
            /* Status code 200 but there was an error while
             * validating the request. */
            _info.error = true;
            _info.result = data.error;
          } else {
            _info.error = false;
            _info.result = data;
          }
          PayExternalStore.emitChange();
        },
        function (err, info) {
          debug('/pay failed', err, info);
          _info.error = true;
          _info.result = err;
          PayExternalStore.emitChange();
        }
      );

      break;
    }

  }

});


module.exports = PayExternalStore;
