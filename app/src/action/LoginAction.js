"use strict";

var AppDispatcher = require('../dispatcher');
var ActionTypes = require('../constant/ActionTypes');
var request = require('../util/request');
var sjcl = require('../util/sjcl');
var debug = require('../debug')(__filename);


function loginOk(data) {
  data.actionType = ActionTypes.LOGIN_OK;
  AppDispatcher.dispatch(data);
};


function loginStatus(data) {
  data.actionType = ActionTypes.LOGIN_STATUS;
  AppDispatcher.dispatch(data);
};


var LoginAction = {

  submit: function(data) {
    /* Login. */
    loginStatus({info: 'login.generatingkey'});

    setTimeout(function() {
      var key = sjcl.genKey(data.username, data.password);
      var uid = sjcl.blobID(key, data.password);

      loginStatus({info: 'login.submitting'});

      request.post('/login', {uid: uid},
        function(obj) {
          if (obj.hi === uid) {
            loginOk({
              username: data.username,
              uid: uid,
              key: key,
              blob: obj.blob,
              usdbalance: obj.usdbalance
            });
          } else {
            loginStatus({info: 'error.login.creds', error: true});
          }
        },
        function(error, res) {
          /* Failed to authenticate. */
          setTimeout(function() {
            /* Note: "error" is a key from a locale. */
            if (res && res.status !== 500) {
              loginStatus({info: 'error.login.creds', error: true});
            } else {
              /* Severe error, maybe the server is not running. */
              debug(error, res);
              loginStatus({info: 'error.server_comm', error: true});
            }
          }, 250);
        }
      );

    }, 250);

  },

  logout: function() {
    AppDispatcher.dispatch({actionType: ActionTypes.LOGOUT});
  }

};


module.exports = LoginAction;
