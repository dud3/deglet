"use strict";

var AppDispatcher = require('../dispatcher');
var ActionTypes = require('../constant/ActionTypes');


var InvoiceAction = {

  load: function(iid) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.INVOICE_LOAD,
      method: 'get',
      id: iid
    });
  },

  refresh: function(iid) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.INVOICE_REFRESH,
      id: iid
    });
  },

  loadRate: function(iid) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.INVOICE_LOAD,
      method: 'post',
      id: iid
    });
  },

  create: function(data) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.INVOICE_CREATE,
      data: data
    });
  }

};


module.exports = InvoiceAction;
