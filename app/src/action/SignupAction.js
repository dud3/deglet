"use strict";

var AppDispatcher = require('../dispatcher');
var ActionTypes = require('../constant/ActionTypes');
var request = require('../util/request');
var sjcl = require('../util/sjcl');
var errorkey = require('../util/errorkey');
var pwdMinlen = require('../constant/App').pwdMinlen;
var debug = require('../debug')(__filename);


function signupOk(data) {
  data.actionType = ActionTypes.LOGIN_OK;
  data.newuser = true;
  AppDispatcher.dispatch(data);
};


function signupStatus(data) {
  data.actionType = ActionTypes.LOGIN_STATUS;
  AppDispatcher.dispatch(data);
};


var SignupAction = {

  submit: function(data) {
    /* Sign up. */
    if (data.password.length < pwdMinlen) {
      signupStatus({info: 'login.weakpwd', error: true});
      return;
    }

    if (data.password !== data.passwordrepeat) {
      signupStatus({info: 'error.login.passwordmismatch', error: true});
      return;
    }

    signupStatus({info: 'login.generatingkey'});

    setTimeout(function() {
      var key = sjcl.genKey(data.username, data.password);
      var uid = sjcl.blobID(key, data.password);

      signupStatus({info: 'login.submitting'});

      request.post(
        '/signup', {uid: uid, username: data.username},
        function(obj) {
          if (obj.error || obj.hi !== uid) {
            var info = (obj.error ?
                        errorkey.deglet(obj) :
                        'error.login.creds');
            signupStatus({info: info, error: true});
          } else {
            /* Success! */
            signupOk({
              username: data.username,
              uid: uid,
              key: key,
              blob: obj.blob,
              usdbalance: obj.usdbalance
            });
          }
        },
        function(error, res) {
          /* Failed to authenticate. */
          setTimeout(function() {
            /* Note: "error" is a key from a locale. */
            if (res && res.status !== 500) {
              signupStatus({info: 'error.login.creds', error: true});
            } else {
              /* Severe error, maybe the server is not running. */
              debug(error, res);
              signupStatus({info: 'error.server_comm', error: true});
            }
          }, 250);
        }
      );

    }, 250);

  }

};


module.exports = SignupAction;
