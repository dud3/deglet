"use strict";

var AppDispatcher = require('../dispatcher');
var ActionTypes = require('../constant/ActionTypes');


var ConvertAction = {

  create: function(uid, address, side, amount, currency, otherCurrency) {
    var data = {
      uid: uid,
      address: address,
      side: side,
      amount: amount,
      currency: currency,
      otherCurrency: otherCurrency
    };

    AppDispatcher.dispatch({
      actionType: ActionTypes.CONVERT_CREATE,
      data: data
    });
  }

};


module.exports = ConvertAction;
