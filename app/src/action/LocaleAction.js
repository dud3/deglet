"use strict";

var AppDispatcher = require('../dispatcher');
var ActionTypes = require('../constant/ActionTypes');


var LocaleAction = {

  change: function(code) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.LOCALE_CHANGE,
      locale: code
    });
  }

};


module.exports = LocaleAction;
