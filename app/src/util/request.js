"use strict";

var request = require('superagent');
var ServerConstants = require('../constant/ServerConstants');


function _request(method, opts, callback, errback) {
  var baseUrl = opts.server || ServerConstants.AUTH_URL;
  var basicAuth = opts.data ? opts.data.auth : null;
  var query = opts.data ? opts.data.query : null;
  var partial;

  if (basicAuth) {
    delete opts.data.auth;
  }
  if (query) {
    delete opts.data.query;
  }

  partial = method(baseUrl + opts.url)
    .type('application/json')
    .accept('application/json')
    .send(opts.data);

  if (query) {
    partial = partial.query(query);
  }

  if (basicAuth) {
    partial = partial.auth(basicAuth.user, basicAuth.password);
  }

  partial.end(function(err, res) {
    if (!err) {
      callback(res.body);
    } else {
      if (errback) {
        errback(err, res);
      }
    }
  });
}


function getBlockchainTx(txid, callback, errback) {
  var opts = {
    server: ServerConstants.INSIGHT_URL,
    url: '/tx/' + txid
  };
  _request(request.get, opts, callback, errback);
}


module.exports = {

  post: function(url, data, callback, errback) {
    var opts = {url: url, data: data};
    _request(request.post, opts, callback, errback);
  },

  put: function(url, data, callback, errback) {
    var opts = {url: url, data: data};
    _request(request.put, opts, callback, errback);
  },

  get: function(url, data, callback, errback) {
    var opts = {url: url, data: data};
    _request(request.get, opts, callback, errback);
  },

  getBlockchainTx: getBlockchainTx

};
