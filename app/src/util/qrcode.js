"use strict";

var qrcode = require('qrcode-js');

var QR_TYPENUMBER = 6;
var QR_CELLSIZE = 5;
var QR_MARGIN = QR_CELLSIZE * 4;
var QR_MODULECOUNT = QR_TYPENUMBER * 4 + 17;
var QR_IMGSIZE = (QR_MODULECOUNT * QR_CELLSIZE + QR_MARGIN * 2) + 'px';

qrcode.typeNumber = QR_TYPENUMBER;
qrcode.errorCorrectLevel = 'Q';


function _buildBitcoinQR(addr, amount, protocol) {
  var url = (typeof protocol === 'string') ? protocol : 'bitcoin:';
  url += addr;
  if (amount) {
    url += '?amount=' + amount;
  }

  var addressQR = qrcode.toDataURL(url, QR_CELLSIZE);
  return {img: addressQR, url: url};
}


module.exports = {
  buildBitcoinQR: _buildBitcoinQR,
  imgSize: QR_IMGSIZE
};
