"use strict";

var Bitcore = require('bitcore-wallet-api').Bitcore;


function validBitcoinAddress(address, network) {
  try {
    return new Bitcore.Address(address, network);
  } catch (e) {
    return null;
  }
}


module.exports = {
  bitcoinAddress: validBitcoinAddress
};
