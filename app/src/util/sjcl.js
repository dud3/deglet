"use strict";

var sjcl = require('bitcore-wallet-api').sjcl;
var params = require('../constant/App').keyparams;


function genKey(username, password) {
  if (!username || !password) {
    throw new Error("credentials not present");
  }
  /* Encryption key of 256 bits generated using PBKDF2-HMAC-SHA256.
   * The server never receives this. */
  var key = sjcl.misc.pbkdf2(password, username, params.iter);
  var hex = sjcl.codec.hex.fromBits(key);
  return hex;
}

function blobID(hexKey, password) {
  if (!hexKey || !password) {
    throw new Error("credentials not present");
  }
  /* Use password as the salt to iterate one time from
   * the encryption key to generate the blob id.
   * The server receives this during login and initial wallet setup.
   *
   * It's expected that hexKey = genKey(...)
   */
  var key = sjcl.codec.hex.toBits(hexKey);
  var id = sjcl.misc.pbkdf2(key, password, 1);

  var hex = sjcl.codec.hex.fromBits(id);
  return hex;
}

function encrypt(hexKey, blob) {
  if (!hexKey) {
    throw new Error("credentials not present");
  }
  /* Encrypt using AES-128-GCM. */
  var key = sjcl.codec.hex.toBits(hexKey);
  var enc = sjcl.encrypt(key, blob, params);
  return enc;
}

function decrypt(hexKey, blob) {
  if (!hexKey) {
    throw new Error("credentials not present");
  }
  /* Decrypt using AES-128-GCM. */
  var key = sjcl.codec.hex.toBits(hexKey);
  var dec = sjcl.decrypt(key, blob, params);
  return dec;
}

function randomHex(numWords) {
  /* Generate 32 bits * numWords using SJCL's PRNG and encode
   * the result as a hexadecimal string.
   *
   * numWords must be at least 1 (which produces a string of length 8)
   */
  var nw = parseInt(numWords);
  if (nw < 1) {
    throw Error("numWords < 1");
  }
  var ran = sjcl.random.randomWords(nw, 6);
  var hex = sjcl.codec.hex.fromBits(ran);
  return hex;
}


module.exports = {
  encrypt: encrypt,
  decrypt: decrypt,
  randomHex: randomHex,
  blobID: blobID,
  genKey: genKey
};
