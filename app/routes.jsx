"use strict";

var React = require('react');
var Router = require('react-router');
var Route = Router.Route;

var AppLinks = require('./src/constant/AppLinks');
var App = require('./src/component/App.react');
var WelcomeSection = require('./src/component/section/Welcome.react');
var Account = require('./src/component/Account.react');
var Invoice = require('./src/component/widget/Invoice.react');
var NotFound = require('./src/component/section/NotFound.react');

var Wallet = require('./src/component/section/Wallet.react');
var LoginForm = require('./src/component/section/LoginForm.react');
var SignupForm = require('./src/component/section/SignupForm.react');
var Receive = require('./src/component/section/Receive.react');
var ReceiveSimple = require('./src/component/section/ReceiveSimple.react');
var ReceiveInvoice = require('./src/component/section/ReceiveInvoice.react');
var Send = require('./src/component/section/Send.react');
var Convert = require('./src/component/section/Convert.react');
var History = require('./src/component/section/History.react');
var HistoryBitcoin = require('./src/component/section/HistoryBitcoin.react');
var HistoryExternal = require('./src/component/section/HistoryExternal.react');
var Settings = require('./src/component/section/Settings.react');
var TxDetail = require('./src/component/section/TxDetail.react');
var Logout = require('./src/component/Logout.react');


module.exports = (
  <Route handler={App}>
    <Route name={AppLinks.name.index} path="/" handler={WelcomeSection}>
      <Router.DefaultRoute name="signup" handler={SignupForm}/>
      <Route name="login" handler={LoginForm}/>
    </Route>
    <Route handler={Account}>
      <Route name={AppLinks.name.wallet} handler={Wallet}/>
      <Route name="send" handler={Send}/>
      <Route name="receive" handler={Receive}>
        <Route name="recvinvoice" handler={ReceiveInvoice}/>
        <Router.DefaultRoute name="recvsimple" handler={ReceiveSimple}/>
      </Route>
      <Route name="convert" handler={Convert}/>
      <Route name="conversion" path="conversion/:id" handler={Invoice} />
      <Route name="history" handler={History}>
        <Route name="histexternal" handler={HistoryExternal}/>
        <Router.DefaultRoute name="histbitcoin" handler={HistoryBitcoin}/>
      </Route>
      <Route name="settings" handler={Settings}/>
      <Route name="txdetail" path="/tx/:txid" handler={TxDetail}/>
      <Route name="logout" handler={Logout}/>
    </Route>
    <Route name="invoice" path="invoice/:id" handler={Invoice} />
    <Router.NotFoundRoute handler={NotFound} />
  </Route>
);
