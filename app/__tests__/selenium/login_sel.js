var test = require('selenium-webdriver/testing');
var expect = require('chai').expect;
var base = require('./base');
var data = require('./data');

var tosAgree = '.loginarea .col-xs-12 h5';
var enLink = '//footer//a[contains(., "English")]';
var ptBRLink = '//footer//a[contains(., "Brasil")]';


test.describe('Deglet - login', function() {

  test.it('shoud load index with login active', function(done) {
    this.driver.get(base.baseUrl + 'login');

    expect(this.driver.getCurrentUrl()).to.eventually.equal(base.baseUrl + 'login');
    expect(this.driver.getTitle()).to.eventually.equal(base.siteTitle);

    expect('.loginarea').dom.to.be.visible();
    expect('.loginarea a[class$=active]').dom.to.have.text('Log In');

    done();
  });

  test.it('should have a form with username and password', function(done) {
    expect('.loginarea form').dom.to.be.visible();
    expect('input[name="username"]').dom.to.be.visible();
    expect('input[name="password"]').dom.to.be.visible();
    expect('button[type="submit"]').dom.to.be.visible();
    done();
  });

  test.it('should submit the form', function(done) {
    base.fill(this.driver, data.user);
    base.submit(this.driver);
    done();
  });

  test.it('should move user to the wallet page', function(done) {
    var driver = this.driver;
    driver.wait(function() {
      return driver.isElementPresent({css: '.site-content.user'});
    });

    expect(driver.getCurrentUrl()).to.eventually.equal(base.baseUrl + 'wallet');
    expect('.horizmenu a[class$="active"]').dom.to.have.text('Home');
    var hello = driver.findElement({xpath: '//span[contains(., "Hello")]'});
    expect(hello.getText()).to.eventually.equal('Hello ' + data.user.username);

    done();
  });

  test.it('should load the "Current receiving address" widget', function(done) {
    var address;
    var driver = this.driver;

    expect('.wallet-address input').dom.to.be.visible();
    var elem = driver.findElement({css: '.wallet-address input'});
    driver.wait(function() {
      return elem.getAttribute('title').then(function(addy) {
        address = addy;
        return address !== '';
      });
    }).then(function() {
      expect(address).to.have.length.within(25, 35);
    });

    done();
  });

  test.it('should toggle balance display', function(done) {
    expect('.balance tbody tr').dom.to.be.visible();

    var hideBtn = '//div[@class="balance-link"]//a[contains(., "Hide")]';
    this.driver.findElement({xpath: hideBtn}).click();
    expect('.balance tbody tr').dom.not.to.be.visible();

    var showBtn = '//div[@class="balance-link"]//a[contains(., "Show")]';
    this.driver.findElement({xpath: showBtn}).click();
    expect('.balance tbody tr').dom.to.be.visible();

    done();
  });

  test.it('should display 2 balances', function(done) {
    var driver = this.driver;
    expect('.balance tbody tr').dom.to.be.visible();
    driver.wait(function() {
      return driver.findElements({css: '.balance tbody tr'}).then(function(result) {
        return result.length === 2;
      });
    });

    done();
  });

  test.it('should log out and go back to the log in page', function(done) {
    base.logout(this.driver, done);
  });

});
