var baseUrl = require('../../src/constant/ServerConstants').THIS_URL;

/* Append a '/' to the url if it is not present. */
baseUrl = baseUrl.replace(/\/?$/, '/');


function waitForText(driver, selector, elemText, done) {
  driver.wait(function() {
    var elem = driver.findElement(selector);
    return elem.getText().then(function(text) {
      return text == elemText;
    });
  }).thenFinally(function() {
    done();
  });
}


function moveTo(driver, linkPath, linkText, done) {
  driver.findElement({css: '.horizmenu a[href$="' + linkPath + '"]'}).click();
  waitForText(driver, {css: '.horizmenu a[class$="active"]'}, linkText, done);
}


function moveToTab(driver, linkPath, linkText, done) {
  driver.findElement({css: '.user-tabs a[href$="' + linkPath + '"]'}).click();
  waitForText(driver, {css: '.user-tabs a[class$="active"]'}, linkText, done);
}


module.exports = {
  baseUrl: baseUrl,
  siteTitle: 'Deglet',
  waitForText: waitForText,
  moveTo: moveTo,
  moveToTab: moveToTab,

  login: function(driver, util, data, done) {
    driver.get(baseUrl + 'login');
    waitForText(driver, {css: '.loginarea a[class$=active]'}, 'Log In', done);
    util.fill(driver, data);
    util.submit(driver);
    driver.wait(function() {
      return driver.isElementPresent({css: '.site-content.user'});
    });
    done();
  },

  logout: function(driver, done) {
    driver.findElement({css: '.menu-links a[href$="/logout"]'}).click();
    driver.wait(function() {
      return driver.isElementPresent({css: '.loginarea form'});
    });
    driver.wait(function() {
      return driver.getCurrentUrl().then(function(url) {
        return url === baseUrl + 'login';
      })
    });
    done();
  }
};
