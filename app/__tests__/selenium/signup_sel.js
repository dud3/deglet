var test = require('selenium-webdriver/testing');
var expect = require('chai').expect;
var base = require('./base');

var user = {
  username: base.randomString(),
  password: base.randomString(12)
};
user.passwordrepeat = user.password;


test.describe('Deglet - sign up', function() {

  test.it('should be displaying the sign up form', function(done) {
    this.driver.get(base.baseUrl);
    expect(this.driver.getCurrentUrl()).to.eventually.equal(base.baseUrl);

    expect('.loginarea a[class$=active]').dom.to.have.text('Sign Up');
    expect('input[name="username"]').dom.to.be.visible();
    expect('input[name="password"]').dom.to.be.visible();
    expect('input[name="passwordrepeat"]').dom.to.be.visible();
    expect('button[type="submit"]').dom.to.be.visible();
    done();
  });

  test.it('should create a new user', function(done) {
    base.fill(this.driver, user);
    base.submit(this.driver);
    done();
  });

  test.it('should move user to the wallet page', function(done) {
    var driver = this.driver;
    driver.wait(function() {
      return driver.isElementPresent({css: '.site-content.user'});
    });

    expect(driver.getCurrentUrl()).to.eventually.equal(base.baseUrl + 'wallet');
    expect('.horizmenu a[class$="active"]').dom.to.have.text('Home');
    var hello = driver.findElement({xpath: '//span[contains(., "Hello")]'});
    expect(hello.getText()).to.eventually.equal('Hello ' + user.username);

    done();
  });

  test.it('should log out and go back to the log in page', function(done) {
    base.logout(this.driver, done);
  });

});
