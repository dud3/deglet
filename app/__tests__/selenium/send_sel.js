var until = require('selenium-webdriver').until;
var test = require('selenium-webdriver/testing');
var expect = require('chai').expect;
var base = require('./base');
var data = require('./data');


var pendingTxList = '.pendingtx tbody tr[class=pendingtx-row]';


function startWithdraw(driver, txCount, data, done) {
  base.fill(driver, data);
  base.submit(driver);
  driver.wait(until.elementIsVisible(driver.findElement({css: '.info-box'})));
  expect('.info-box h5').dom.to.contain.text('Transaction created');

  driver.wait(function() {
    /* Wait for a new element to show up in the pending tx table. */
    return driver.findElements({css: pendingTxList}).then(function(result) {
      return result.length === txCount + 1;
    });
  }, 10000).thenFinally(function() {
    done();
  });
}


function actionLastTrans(driver, confirm, txCount, amount, done) {
  var trans = '.pendingtx tbody tr[class=pendingtx-row]:last-child';
  var btn = trans + ' a:' + (confirm ? 'last-child' : 'first-child');
  expect(trans + ' td[data-label="Amount"]').dom.to.have.text(amount);

  // Perform action.
  expect(btn).dom.to.have.text(confirm ? 'Confirm' : 'Reject');
  driver.findElement({css: btn}).click();

  driver.wait(function() {
    /* Wait for an element to be removed from the pending tx table. */
    return driver.findElements({css: pendingTxList}).then(function(result) {
      return result.length == txCount;
    });
  }, 10000).thenFinally(function() {
    done();
  });
  done();
}


test.describe('Deglet - send a transaction', function() {

  var address;
  var txCount = -1;
  var minBalance = 0.0002;  // Fail if the balance available is less than this.
  var balance = {
    total: null,
    available: null
  };

  test.it('shoud login successfully', function(done) {
    base.login(this.driver, base, data.user, done);
  });

  test.it('should fetch the current receiving address', function(done) {
    var driver = this.driver;
    var elem = driver.findElement({css: '.wallet-address input'});
    driver.wait(function() {
      return elem.getAttribute('title').then(function(addy) {
        address = addy;
        return address !== '';
      });
    });
    done();
  });

  test.it('should move to the Send page', function(done) {
    base.moveTo(this.driver, '/send', 'Send', done);
  });

  test.it('should fetch the bitcoin balance', function(done) {
    var driver = this.driver;
    driver.wait(function() {
      return driver.isElementPresent({css: '.balance tbody tr[data-reactid$=balance-BTC]'});
    });

    driver.findElement({xpath: '(//div[@class="balance"]//td)[1]'}).getText().then(function(text) {
      balance.total = parseFloat(text);
    });
    driver.findElement({xpath: '(//div[@class="balance"]//td)[2]'}).getText().then(function(text) {
      balance.available = parseFloat(text);
    });

    done();
  });

  test.it('should have a balance greater than ' + minBalance + ' BTC', function(done) {
    expect(balance.available).to.be.above(minBalance);
    done();
  });

  test.it('should fail to send to an invalid address', function(done) {
    base.fill(this.driver, {'address': '123', 'amount': '0.01'});
    base.submit(this.driver);
    this.driver.wait(until.elementLocated({css: '.error-box'}));
    expect('.error-box h5').dom.to.be.visible();
    expect('.error-box h5').dom.to.contain.text('is invalid');
    /* Close the message box. */
    this.driver.findElement({css: '.box-msg button'}).click();

    done();
  });

  test.it('should fail to send an amount greater than total balance', function(done) {
    base.fill(this.driver, {'address': address, 'amount': balance.total + 0.0001});
    base.submit(this.driver);
    this.driver.wait(until.elementLocated({css: '.error-box'}));
    expect('.error-box h5').dom.to.contain.text('above your balance');
    this.driver.findElement({css: '.box-msg button'}).click();

    done();
  });

  test.it('should fail to send all the available balance due to the network fee', function(done) {
    base.fill(this.driver, {'address': address, 'amount': balance.available});
    base.submit(this.driver);
    this.driver.wait(until.elementLocated({css: '.error-box'}));
    expect('.error-box h5').dom.to.contain.text('network fee');
    this.driver.findElement({css: '.box-msg button'}).click();

    done();
  });

  test.it('should find the number of pending transactions', function(done) {
    expect('.pendingtx tbody').dom.to.be.visible();

    this.driver.findElements({css: pendingTxList}).then(function(result) {
      txCount = result.length;
    }).then(function() {
      expect(txCount).to.be.above(-1);
    });

    done();
  });

  test.it('should succeed to start a withdrawal', function(done) {
    startWithdraw(this.driver, txCount, {'address': address, 'amount': '0.0001'}, done);
  });

  test.it('should reject the last transaction created', function(done) {
    actionLastTrans(this.driver, false, txCount, '0.0001 BTC', done);
  });

  test.it('should succeed to start a second withdrawal', function(done) {
    startWithdraw(this.driver, txCount, {'address': address, 'amount': '0.00011'}, done);
  });

  test.it('should confirm the last transaction created', function(done) {
    actionLastTrans(this.driver, true, txCount, '0.00011 BTC', done);
  });

  test.it('should log out', function(done) {
    base.logout(this.driver, done);
  });

});
