var test = require('selenium-webdriver/testing');
var expect = require('chai').expect;
var base = require('./base');

var tosAgree = '.loginarea .col-xs-12 h5';
var enLink = '//footer//a[contains(., "English")]';
var ptBRLink = '//footer//a[contains(., "Brasil")]';


test.describe('Deglet - index', function() {

  test.it('shoud load index with signup active', function(done) {
    this.driver.get(base.baseUrl);

    expect(this.driver.getCurrentUrl()).to.eventually.equal(base.baseUrl);
    expect(this.driver.getTitle()).to.eventually.equal(base.siteTitle);

    expect('.loginarea').dom.to.be.visible();
    expect('.loginarea a[class$=active]').dom.to.have.text('Sign Up');

    done();
  });

  test.it('should be in English', function(done) {
    expect('.footer .lang a[class$=active]').dom.to.have.text('English');
    expect(tosAgree).dom.to.contain.text('our Terms of service');
    done();
  });

  test.it('should switch to Portuguese', function(done) {
    var link = this.driver.findElement({xpath: ptBRLink});
    link.click();

    expect('.footer .lang a[class$=active]').dom.to.have.text(
      'Português (Brasil)');
    expect(tosAgree).dom.to.contain.text('nossos Termos de Servi');

    done();
  });

  test.it('should switch back to English', function(done) {
    this.driver.findElement({xpath: enLink}).click();
    expect('.footer .lang a[class$=active]').dom.to.have.text('English');
    done();
  });

  test.it('should redirect to / when visiting /wallet without an user', function(done) {
    this.driver.get(base.baseUrl + 'wallet');
    expect(this.driver.getCurrentUrl()).to.eventually.equal(base.baseUrl);
    done();
  });

});
